package team1.logistic.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import team1.logistic.entity.PackageVO;
import team1.logistic.reponsitory.PackageRepository;

/**
 * 
 * @author An
 * 
 * */
@Service("companyAPIService")
public class CompanyAPIService {
	enum north {HOA_BINH, SON_LA, DIEN_BIEN, LAI_CHAU, LAO_CAI, YEN_BAI, PHU_THO,
				HA_GIANG, TUYEN_QUANG, CAO_BANG, BAC_KAN, THAI_NGUYEN, LANG_SON, BAC_GIANG, 
				QUANG_NINH, HA_NOI, BAC_NINH, HA_NAM, HAI_DUONG, HAI_PHONG, HUNG_YEN, NAM_DINH, 
				THAI_BINH, VINH_PHUC, NINH_BINH  };
	enum central { THANH_HOA, NGHE_AN, HA_TINH, QUANG_BINH, QUANG_TRI, THUA_THIEN_HUE, DA_NANG,
				QUANG_NAM, QUANG_NGAI, BINH_DINH, PHU_YEN, KHANH_HOA, NINH_THUAN, BINH_THUAN,
				KON_TUM, GIA_LAI, DAK_LAK, DAK_NONG, LAM_DONG};
	enum south { TPHCM, BA_RIA_VUNG_TAU, BINH_DUONG, BINH_PHUOC, DONG_NAI, TAY_NINH, AN_GIANG,
				BAC_LIEU, BEN_TRE, CA_MAU, CAN_THO, DONG_THAP, HAU_GIANG, KIEN_GIANG, LONG_AN,
				SOC_TRANG, TIEN_GIANG, TRA_VINH, VINH_LONG};
	private static Map<String,Integer> location = new HashMap<String,Integer>();
	
	@Autowired
	private PackageRepository pkg_repo;
	
//	private static String test = "{ \"pkg_id\" : \"123\",\"weight\": 2.0 , \"size\": \"vSmall\","
//			+ " \"delivery_fee\" : 10000 , \"cod_value\" : 0 "
//			+ " \"sender_name\": \"an\", \"sender_phone_num\": \"123456789\", \"sender_address\": \"Soc Trang\""
//			+ " \"receiver_name\": \"thao\", \"receiver_phone_num\": \"987654321\", \"receiver_address\": \"can tho\"}";
	
	
	
	
	public CompanyAPIService() {
		super();
		for (north a: north.values()) {
			String b = a.toString().replace("_", "");
			b = b.toLowerCase();
            location.put(b, 1);
        }
		for (central a: central.values()) {
			String b = a.toString().replace("_", "");
			b = b.toLowerCase();
            location.put(b, 2);
        }
		for (south a: south.values()) {
			String b = a.toString().replace("_", "");
			b = b.toLowerCase();
            location.put(b, 3);
        }
		
	}
	
	public String findTracking (String pkg_id) {
		String result = pkg_repo.getById(pkg_id).getTracking_status();
		return result;
	}
	
	public int calculateFee (PackageVO pkg) {

		Double fee;
		Double size_fix = 0.0;
		Double weight = 0.0;

		String sender = pkg.getSender_address()
				.substring(10, pkg.getSender_address().indexOf("District")).toLowerCase().trim();
		String receiver = pkg.getReceiver_address()
				.substring(10, pkg.getReceiver_address().indexOf("District")).toLowerCase().trim();
		
		if (location.get(sender)!=null && location.get(receiver)!=null) {
		if (sender.compareTo(receiver)!=0) {
			if (location.get(sender).toString().equals(location.get(receiver).toString())) {
				fee = 20000.0;
			} else {
				fee = 30000.0;
			}
		} else {
			fee=10000.0;
		}
		} else {
			fee = -1.0;
		}
		if (pkg.getSize()!=null) {
			size_fix= -1.0;
		switch (pkg.getSize()) {
			case "vBig": size_fix = 2.5 ; break;
			case "Big": size_fix = 2.1; break;
			case "Medium": size_fix = 1.8 ; break;
			case "Small": size_fix = 1.4 ; break;
			case "vSmall": size_fix = 1.0 ; break;
		}
		} else {
			size_fix= -1.0;
		}
		try {
		weight = pkg.getWeight();
		if (weight<1.0) weight = 1.0;
		}
		catch (Exception e) {
			weight = -1.0;
		}
		weight = Math.sqrt(weight);
		
		fee = fee*size_fix*weight;
		System.out.println(fee);
		System.out.println(size_fix);
		System.out.println(weight);
		
		
		return fee.intValue() ;
	}
	
	public Boolean savePackage (PackageVO pkg) {
		pkg.setTracking_status("new");
		try {
		pkg_repo.save(pkg);
		} catch (Exception e) {
			return false;
		}
		return true;
	}
	
}
