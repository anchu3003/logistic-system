package team1.logistic.service;

import java.util.HashSet;
import java.util.Set;
import java.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import team1.logistic.entity.PackageVO;
import team1.logistic.entity.RouteVO;
import team1.logistic.reponsitory.PackageRepository;
import team1.logistic.reponsitory.RouteRepository;
/**
 * @author Long
 */
@Service("driverService")
public class DriverService {
	
	@Autowired
	private RouteRepository routeRepo;
	@Autowired
	private PackageRepository pkgRepo;

	public Object getPackageListToday(int driver_id) {
		RouteVO result = new RouteVO();

		for (RouteVO route : routeRepo.findAll()) {
			if ((route.getStart_datetime().getDayOfMonth() == LocalDateTime.now().getDayOfMonth())
					&& (route.getDriver_id() == driver_id)) {
				result = route;
			}
		}
		return result;
	}

	public Set<RouteVO> getDriverHistory(int driver_id) {

		Set<RouteVO> result1 = new HashSet<RouteVO>();
		for (RouteVO route : routeRepo.findAll()) {
			if ((route.getStart_datetime().getMonth() == LocalDateTime.now().getMonth())
					&& (route.getDriver_id() == driver_id)) {
				result1.add(route);
			}
		}
		return result1;
	}

	// send request: package list has arrived to hub
	public Object sendTracking(String pkg_id, int driver_id) {
		try {
			PackageVO pkg = pkgRepo.getById(pkg_id);
			if (pkg.getTracking_status().equals("sending")) {
				pkgRepo.saveAndFlush(pkg);
			} else {
				pkg.setTracking_status("sending");
				pkgRepo.saveAndFlush(pkg);
			}
		} catch (Exception e) {
			return false;
		}
		return true;
	}
}

	




