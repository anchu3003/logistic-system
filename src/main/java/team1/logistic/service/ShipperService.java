package team1.logistic.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import team1.logistic.entity.PackageVO;
import team1.logistic.entity.ShipRouteVO;
import team1.logistic.reponsitory.PackageRepository;
import team1.logistic.reponsitory.ShipRouteVORepository;


/**
 * @author Huan
 *
 */
@Service("shipperService")
public class ShipperService {

	@Autowired
	private PackageRepository packageRepo;
	@Autowired
	private ShipRouteVORepository shipRouteRepo;

	// Show packagelist today
//	public ShipRouteVO getPackageListToday(int shipper_id) {
//		ShipRouteVO result = new ShipRouteVO();
//		for (ShipRouteVO route : shipRouteRepo.findAll()) {
//			if ((route.getDate().getDayOfMonth() == LocalDate.now().getDayOfMonth())
//					&& (route.getShipper_id() == shipper_id)) {
//				result = route;
//			}
//		}
//		return result;
//
//	}
	
	// PackageToday Tra ve PackageVO
	public Set<PackageVO> getPackageToday(int shipper_id) {
		Set<PackageVO> result = new HashSet<PackageVO>();
		
		for (ShipRouteVO route : shipRouteRepo.findAll()) {
			if ((route.getDate().getDayOfMonth() == LocalDate.now().getDayOfMonth())
					&& (route.getShipper_id() == shipper_id)) {
				result.addAll(route.getPackagelist());
			}
		}
		return result;

	}

	// Show packagelist history
	public Set<ShipRouteVO> getPackageHistory(int shipper_id) {
		Set<ShipRouteVO> result = new HashSet<ShipRouteVO>();
		for (ShipRouteVO route : shipRouteRepo.findAll()) {
			if ((route.getDate().getMonth() == LocalDate.now().getMonth()) 
					&& (route.getShipper_id() == shipper_id)) {
				result.add(route);
			}
		}
		return result;

	}
	//history tra ve PackageVO
//	public Set<PackageVO> getPackageHistory(int shipper_id) {
//		Set<PackageVO> result = new HashSet<PackageVO>();
//		
//		for (ShipRouteVO route : shipRouteRepo.findAll()) {
//			if (route.getDate().getMonth() == LocalDate.now().getMonth()
//					&& (route.getShipper_id() == shipper_id)) {
//				result.addAll(route.getPackagelist());
//			}
//		}
//		return result;
//
//	}
//	

//Show packagelist by shipper id
	public Set<ShipRouteVO> getPackageList(int shipper_id) {
		Set<ShipRouteVO> result = new HashSet<ShipRouteVO>();
		for (ShipRouteVO route : shipRouteRepo.findAll()) {
			if (route.getShipper_id() == shipper_id) {

				result.add(route);
			}
		}
		return result;

	}

// pick package
	public Boolean pickPackageById(String pkg_id, int shipper_id) {
		try {
			System.out.println(packageRepo.getById(pkg_id));
			PackageVO pkg = packageRepo.getById(pkg_id);
			if (pkg.getCurrent_shipper() == (-1)) {
				pkg.setTracking_status("picking");
				pkg.setCurrent_shipper(shipper_id);
				pkg.setPick_time(LocalDateTime.now());
			} else {
				pkg.setTracking_status("delivering");
			}
			packageRepo.saveAndFlush(pkg);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

// drop package
	public Object dropPackageById(String pkg_id, int shipper_id) {
		try {
			PackageVO pkg = packageRepo.getById(pkg_id);
			System.out.println(pkg);
			if (pkg.getTracking_status() == "picking") {
				pkg.setTracking_status("startpoint");
			} else {
				pkg.setCurrent_shipper(-1);
				pkg.setTracking_status("delivered");
				pkg.setDrop_time(LocalDateTime.now());
			}
			packageRepo.saveAndFlush(pkg);
		} catch (Exception e) {
			return false;
		}
		return true;
	}
}
