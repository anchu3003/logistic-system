package team1.logistic.service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import team1.logistic.entity.DriverVO;
import team1.logistic.entity.PackageVO;
import team1.logistic.entity.RouteVO;
import team1.logistic.reponsitory.DriverRepository;
import team1.logistic.reponsitory.PackageRepository;
import team1.logistic.reponsitory.RouteRepository;

/**
 * @author The Phap
 */

@Service("dividerService")
public class DividerService {
	// DEFINE_LOCATION_PROVINCE
	enum northWest {
		LAOCAI, YENBAI, DIENBIEN, HOABINH, LAICHAU, SONLA
	};

	enum northEast {
		HAGIANG, CAOBANG, BACKAN, LANGSON, TUYENQUANG, THAINGUYEN, PHUTHO, BACGIANG, QUANGNINH
	};

	enum north {
		BACNINH, HANAM, HANOI, HAIDUONG, HAIPHONG, HUNGYEN, NAMDINH, NINHBINH, THAIBINH, VINHPHUC
	};

	enum northCentral {
		THANHHOA, NGHEAN, HATINH, QUANGBINH, QUANGTRI, THUATHIENHUE
	};

	enum southCentral {
		DANANG, QUANGNAM, QUANGNGAI, BINHDINH, PHUYEN, KHANHHOA, NINHTHUAN, BINHTHUAN
	};

	enum highLands {
		KONTUM, GIALAI, DAKLAK, DAKNONG, LAMDONG
	};

	enum southEast {
		HOCHIMINH, BARIAVUNGTAU, BINHDUONG, BINHPHUOC, DONGNAI, TAYNINH
	};

	enum southWest {
		ANGIANG, BACLIEU, BENTRE, CAMAU, CANTHO, DONGTHAP, HAUGIANG, KIENGIANG, LONGAN, SOCTRANG, TIENGIANG,
		TRAVINH, VINHLONG

	};

	private static Map<String, Integer> location = new HashMap<String, Integer>();
	private static List<String> idHub = new ArrayList<String>();

	@Autowired
	private DriverRepository driverRepo;
	@Autowired
	private RouteRepository routeRepo;
	@Autowired
	private PackageRepository pkgRepo;

	public DividerService() {
		super();
		idHub.add("YenBai");
		idHub.add("ThaiNguyen");
		idHub.add("HaNoi");
		idHub.add("NgheAn");
		idHub.add("DaNang");
		idHub.add("LamDong");
		idHub.add("HoChiMinh");
		idHub.add("CanTho");


	}

	// GET_ALL_DRIVER
	public List<DriverVO> getDriverList() {
		return driverRepo.findAll();
	}

	// CHANGE_DRIVER_WORKING
	public void changeDriver(RouteVO routeVO) {
		routeRepo.save(routeVO);
	}

	// GET_ROUTE_TODAY
	public List<RouteVO> getRouteToday() {
		prepareAllRoute();
		List<RouteVO> routeVOList = new ArrayList<RouteVO>();
		for (RouteVO routeVO : routeRepo.findAll()) {
			routeVOList.add(routeVO);
		}
		return routeVOList;
	}

	// PREPARE_ROUTE_HUB
	public void prepareRoute(String lghub_id) {
		List<PackageVO> listVO = pkgRepo.getListPackageInHub(lghub_id);
		List<PackageVO> pkgNS = new ArrayList<PackageVO>();
		List<PackageVO> pkgSN = new ArrayList<PackageVO>();
		for (PackageVO pkgVO : listVO) {
			if (currentLocation(pkgVO) > endLocation(pkgVO)) {
				pkgSN.add(pkgVO);
			} else if (currentLocation(pkgVO) < endLocation(pkgVO)) {
				pkgNS.add(pkgVO);
			}
		}
		if (lghub_id.equalsIgnoreCase("CanTho")) {
			RouteVO routeVO = new RouteVO();
			int driverId = driverRepo.getDriverByStatus(lghub_id).getStaff_id();
			routeVO.setRoute_id(lghub_id + "-" + idHub.get(idHub.indexOf(lghub_id) - 1) + "-" + LocalDateTime.now());
			routeVO.setDriver_id(driverId);
			DriverVO driverVO = driverRepo.getById(driverId);
			driverVO.setStatus("working");
			driverRepo.save(driverVO);
			routeVO.setStart_pos(lghub_id);
			routeVO.setEnd_pos(idHub.get(idHub.indexOf(lghub_id) - 1));
			routeVO.setStart_datetime(LocalDateTime.now());
			Set<PackageVO> pkgSet = new HashSet<PackageVO>();
			for (PackageVO pkg : pkgSN) {
				pkgSet.add(pkg);
			}
			routeVO.setPackagelist(pkgSet);
			routeRepo.save(routeVO);
		} else if (lghub_id.equalsIgnoreCase("YenBai")) {
			RouteVO routeVO = new RouteVO();
			int driverId = driverRepo.getDriverByStatus(lghub_id).getStaff_id();
			routeVO.setRoute_id(lghub_id + "-" + idHub.get(idHub.indexOf(lghub_id) + 1) + "-" + LocalDateTime.now());
			routeVO.setDriver_id(driverId);
			DriverVO driverVO = driverRepo.getById(driverId);
			driverVO.setStatus("working");
			driverRepo.save(driverVO);
			routeVO.setStart_pos(lghub_id);
			routeVO.setEnd_pos(idHub.get(idHub.indexOf(lghub_id) + 1));
			routeVO.setStart_datetime(LocalDateTime.now());
			Set<PackageVO> pkgSet = new HashSet<PackageVO>();
			for (PackageVO pkg : pkgNS) {
				pkgSet.add(pkg);
			}
			routeVO.setPackagelist(pkgSet);
			routeRepo.save(routeVO);
		} else {
			RouteVO routeSN = new RouteVO();
			int driverId = driverRepo.getDriverByStatus(lghub_id).getStaff_id();
			routeSN.setRoute_id(lghub_id + "-" + idHub.get(idHub.indexOf(lghub_id) - 1) + "-" + LocalDateTime.now());
			routeSN.setDriver_id(driverId);
			DriverVO driverVO = driverRepo.getById(driverId);
			driverVO.setStatus("working");
			driverRepo.save(driverVO);
			routeSN.setStart_pos(lghub_id);
			routeSN.setEnd_pos(idHub.get(idHub.indexOf(lghub_id) - 1));
			routeSN.setStart_datetime(LocalDateTime.now());
			Set<PackageVO> pkgSetSN = new HashSet<PackageVO>();
			for (PackageVO pkg : pkgSN) {
				pkgSetSN.add(pkg);
			}
			routeSN.setPackagelist(pkgSetSN);
			routeRepo.save(routeSN);
			RouteVO routeNS = new RouteVO();
			int driverId2 = driverRepo.getDriverByStatus(lghub_id).getStaff_id();
			routeNS.setRoute_id(lghub_id + "-" + idHub.get(idHub.indexOf(lghub_id) + 1) + "-" + LocalDateTime.now());
			routeNS.setDriver_id(driverId2);
			DriverVO driver = driverRepo.getById(driverId2);
			driver.setStatus("working");
			driverRepo.save(driver);
			routeNS.setStart_pos(lghub_id);
			routeNS.setEnd_pos(idHub.get(idHub.indexOf(lghub_id) + 1));
			routeNS.setStart_datetime(LocalDateTime.now());
			Set<PackageVO> pkgSetNS = new HashSet<PackageVO>();
			for (PackageVO pkg : pkgNS) {
				pkgSetNS.add(pkg);
			}
			routeNS.setPackagelist(pkgSetNS);
			routeRepo.save(routeNS);
		}
	}

	// CALL_SET_NEXT_HUB+PREPARE_ROUTE
	@Scheduled(fixedRate = 300000, initialDelay = 2000) // 5 phut
	public void prepareAllRoute() {
		System.out.println("run route");
		if (routeRepo.findAll().isEmpty()) {
			for (int i = 0; i < idHub.size(); i++) {
				setNextHub(idHub.get(i));
				prepareRoute(idHub.get(i));
			}
		}
	}

	
	// SET_NEXT_PACKAGE
	public void setNextHub(String lghub_id) {
		List<PackageVO> listVO = pkgRepo.getListPackageInHub(lghub_id);
		for (PackageVO pkgVO : listVO) {
			if (currentLocation(pkgVO) > endLocation(pkgVO)) {
				pkgVO.setNext_hub(idHub.get(idHub.indexOf(lghub_id) - 1));
				pkgRepo.save(pkgVO);
			} else if (currentLocation(pkgVO) < endLocation(pkgVO)) {
				pkgVO.setNext_hub(idHub.get(idHub.indexOf(lghub_id) + 1));
				pkgRepo.save(pkgVO);
			} else {
				pkgVO.setNext_hub(pkgVO.getCurrent_hub());
				pkgRepo.save(pkgVO);
			}
		}
	}
	

	
	// RECEIVE_LOCACTION
	public Integer endLocation(PackageVO packageVO) {
		createValueArea();
		int result = -1;
		String receiver_province = packageVO.getReceiver_address()
				.substring(10, packageVO.getReceiver_address().indexOf("District")).toLowerCase().trim();
		switch (location.get(receiver_province)) {
		case 0:
			result = 0;
			return result;
		case 1:
			result = 1;
			return result;
		case 2:
			result = 2;
			return result;
		case 3:
			result = 3;
			return result;
		case 4:
			result = 4;
			return result;
		case 5:
			result = 5;
			return result;
		case 6:
			result = 6;
			return result;
		case 7:
			result = 7;
			return result;
		}
		return result;
	}

	// CRURENT_HUB_LOCATION
	public Integer currentLocation(PackageVO packageVO) {
		createValueArea();
		int result = -1;
		String currentHub = packageVO.getCurrent_hub().trim().toLowerCase();
		switch (location.get(currentHub)) {
		case 0:
			result = 0;
			return result;
		case 1:
			result = 1;
			return result;
		case 2:
			result = 2;
			return result;
		case 3:
			result = 3;
			return result;
		case 4:
			result = 4;
			return result;
		case 5:
			result = 5;
			return result;
		case 6:
			result = 6;
			return result;
		case 7:
			result = 7;
			return result;
		}
		return result;

	}

	// MAPPING_LOCATION
	public void createValueArea() {
		// create value location lowerCase when init
		for (northWest pro : northWest.values()) {
			String province = pro.toString().toLowerCase().trim();
			location.put(province, 0);
		}
		for (northEast pro : northEast.values()) {
			String province = pro.toString().toLowerCase().trim();
			location.put(province, 1);
		}
		for (north pro : north.values()) {
			String province = pro.toString().toLowerCase().trim();
			location.put(province, 2);
		}
		for (northCentral pro : northCentral.values()) {
			String province = pro.toString().toLowerCase().trim();
			location.put(province, 3);
		}
		for (southCentral pro : southCentral.values()) {
			String province = pro.toString().toLowerCase().trim();
			location.put(province, 4);
		}
		for (highLands pro : highLands.values()) {
			String province = pro.toString().toLowerCase().trim();
			location.put(province, 5);
		}
		for (southEast pro : southEast.values()) {
			String province = pro.toString().toLowerCase().trim();
			location.put(province, 6);
		}
		for (southWest pro : southWest.values()) {
			String province = pro.toString().toLowerCase().trim();
			location.put(province, 7);
		}
	}

}
