package team1.logistic.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import team1.logistic.entity.LogisticHubVO;
import team1.logistic.entity.PackageVO;
import team1.logistic.entity.RouteVO;
import team1.logistic.entity.ShipRouteVO;
import team1.logistic.entity.ShipperVO;
import team1.logistic.reponsitory.LogisticHubRepository;
import team1.logistic.reponsitory.PackageRepository;
import team1.logistic.reponsitory.RouteRepository;
import team1.logistic.reponsitory.ShipRouteVORepository;
import team1.logistic.reponsitory.ShipperRepository;

/**
 * 
 * @author Canh
 * 
 * */
@Service("hubService")
public class HubService {
	@Autowired
	private ShipperRepository shipperRepo;
	@Autowired
	private RouteRepository routeRepo;
	@Autowired
	private ShipRouteVORepository shiprouteRepo;
	@Autowired
	private PackageRepository pkgRepo;
	@Autowired
	private LogisticHubRepository hub_repo;

	// GET TO DO LIST TODAY IN HUB
	public Object getTodayList(String lghub_id) {
		List<Object> list = new ArrayList<Object>();
		list.add(getImportRouteToday(lghub_id));
		list.add(getExportRouteToday(lghub_id));
		list.add(getShipRouteToday(lghub_id));
		return list;
	}
	
	//# GET IMPORT ROUTE LIST THAT HUB NEED TO WORK TODAY
	private List<RouteVO> getImportRouteToday(String lghub_id) {
		List<RouteVO> allRoute = new ArrayList<RouteVO>();
		List<RouteVO> importRoutesList = new ArrayList<RouteVO>();
		allRoute = routeRepo.findAll();
		for (RouteVO route : allRoute) {
			if ( route.getEnd_pos().equals(lghub_id)) {
				importRoutesList.add(route);
			}
		}
		return importRoutesList;
	}
	
	//# GET SHIP ROUTE LIST THAT HUB NEED TO WORK TODAY
	private List<ShipRouteVO> getShipRouteToday(String lghub_id) {
		List<ShipRouteVO> allShipRoute = new ArrayList<ShipRouteVO>();
		List<ShipRouteVO> shipRouteList = new ArrayList<ShipRouteVO>();
		allShipRoute = shiprouteRepo.findAll();
		for (ShipRouteVO shipRoute: allShipRoute) {
			if (shipRoute.getDate().compareTo(LocalDate.now()) == 0 && checkShipRouteHub(lghub_id, shipRoute)) {
				shipRouteList.add(shipRoute);
			}
		}
		return shipRouteList;
	}
	
	//# CREATE SHIP ROUTE LIST TODAY
	private void createShipRouteToday(String lghub_id) {
		List<PackageVO> pickupPkgList = pkgRepo.getPickupPackageList(lghub_id);
		List<PackageVO> deliveryPkgList = pkgRepo.getDeliveryPackageList(lghub_id);
		List<PackageVO> pkgList = new ArrayList<PackageVO>();
		pkgList.addAll(pickupPkgList);
		pkgList.addAll(deliveryPkgList);
		List<ShipperVO> shipperList = getShippersInHub(lghub_id);
		int packageAVG;
		packageAVG = pkgList.size() / shipperList.size();
		if (packageAVG<1) packageAVG = 1;
		if (pkgList.size() % shipperList.size() != 0) ++packageAVG;
		for (ShipperVO shipper: shipperList) {
			ShipRouteVO shipRoute = new ShipRouteVO();
			shipRoute.setShip_route_id(shipper.getName()+LocalDateTime.now());
			shipRoute.setShipper_id(shipper.getStaff_id());
			shipRoute.setDate(LocalDate.now());
			Set<PackageVO> pkgSet = new HashSet<PackageVO>();
			int pkgCount = 0;
			for (PackageVO pkg: pkgList) {
				if (pkgCount == packageAVG) {
					break;
				}
				if (pkg.getCurrent_shipper() == -1) {
					pkg.setCurrent_shipper(shipper.getStaff_id());
					pkgSet.add(pkg);
					pkgCount++;
				}
			}
			if(!pkgSet.isEmpty()) {
				shipRoute.setPackagelist(pkgSet);
				shiprouteRepo.save(shipRoute);
			}
			
		}
	}
	
	//# CHECK SHIP ROUTE HUB
	private boolean checkShipRouteHub(String lghub_id, ShipRouteVO shipRoute) {	 
		Set<PackageVO> listPkg = new HashSet<PackageVO>();
		listPkg = shipRoute.getPackagelist();
		Iterator<PackageVO> pkgIterator = listPkg.iterator();
		if(pkgIterator.hasNext()) {
			PackageVO pkg = pkgIterator.next();
			if (pkg.getSender_address().contains(lghub_id) || pkg.getSender_address().contains(lghub_id)) {
				return true;
			}
		}	
		return false;
	}
	
	//# GET EXPORT ROUTE LIST THAT HUB NEED TO WORK TODAY
	private List<RouteVO> getExportRouteToday(String lghub_id) {
			List<RouteVO> allRoute = new ArrayList<RouteVO>();
			List<RouteVO> exportRoutesList = new ArrayList<RouteVO>();
			allRoute = routeRepo.findAll();
			for (RouteVO route : allRoute) {
				if ( route.getStart_pos().equals( lghub_id ) && LocalDate.now().compareTo(route.getStart_datetime().toLocalDate()) == 0 ) {
					exportRoutesList.add(route);
				}
			}
			return exportRoutesList;
		}
	
	//# GET IMPORT LIST TO DO IN HUB
	public Object getImportToday(String lghub_id) {
		List<Object> list = new ArrayList<Object>();
		list.add(getImportRouteToday(lghub_id));
		list.add(getShipRouteToday(lghub_id));
		return list;
	}
	
	//# PUT IMPORT BY ROUTE
	public void putImport(RouteVO route, String lghub_id) {
		route = routeRepo.getById(route.getRoute_id());
		Set<PackageVO> listPkg = new HashSet<PackageVO>();
		listPkg = route.getPackagelist();
		Iterator<PackageVO> pkgIterator = listPkg.iterator();
		while (pkgIterator.hasNext()) {
			String pkg_id = pkgIterator.next().getPkg_id();
			PackageVO pkg_temp = pkgRepo.getById(pkg_id);
			pkg_temp.setCurrent_hub(lghub_id);
			pkg_temp.setCurrent_driver(-1);
			pkg_temp.setCurrent_shipper(-1);
			pkgRepo.save(pkg_temp);
		}
	}
	
	//# PUT IMPORT BY SHIP ROUTE
	public void putImport(ShipRouteVO shipRoute, String lghub_id) {
		shipRoute = shiprouteRepo.getById(shipRoute.getShip_route_id());
		Set<PackageVO> listPkg = new HashSet<PackageVO>();
		listPkg = shipRoute.getPackagelist();
		Iterator<PackageVO> pkgIterator = listPkg.iterator();
		while (pkgIterator.hasNext()) {
			String pkg_id = pkgIterator.next().getPkg_id();
			PackageVO pkg_temp = pkgRepo.getById(pkg_id);
			pkg_temp.setCurrent_hub(lghub_id);
			pkg_temp.setCurrent_driver(-1);
			pkg_temp.setCurrent_shipper(-1);
			pkgRepo.save(pkg_temp);
		}
	}
	//# PUT IMPORT BY PACKGE
	public void putImport(PackageVO pkgImport, String lghub_id) {
		pkgImport = pkgRepo.getById(pkgImport.getPkg_id());
		pkgImport.setCurrent_hub(lghub_id);
		pkgRepo.save(pkgImport);
	}
	
	//# GET EXPORT LIST TO DO IN HUB
	public Object getExportToday(String lghub_id) {
		List<Object> list = new ArrayList<Object>();
		list.add(getExportRouteToday(lghub_id));
		list.add(getShipRouteToday(lghub_id));
		return list;
	}
		
	//# PUT EXPORT BY ROUTE
	public void putExport(RouteVO route) {
		route = routeRepo.getById(route.getRoute_id());
		Set<PackageVO> listPkg = new HashSet<PackageVO>();
		listPkg = route.getPackagelist();
		int driverID = route.getDriver_id();
		Iterator<PackageVO> pkgIterator = listPkg.iterator();
		while (pkgIterator.hasNext()) {
			String pkg_id = pkgIterator.next().getPkg_id();
			PackageVO pkg_temp = pkgRepo.getById(pkg_id);
			pkg_temp.setCurrent_hub("-1");
			pkg_temp.setCurrent_driver(driverID);
			pkgRepo.save(pkg_temp);
		}
	}
	
	//# PUT EXPORT BY SHIP ROUTE
	public void putExport(ShipRouteVO shiproute) {
		shiproute = shiprouteRepo.getById(shiproute.getShip_route_id());
		Set<PackageVO> listPkg = new HashSet<PackageVO>();
		listPkg = shiproute.getPackagelist();
		Iterator<PackageVO> pkgIterator = listPkg.iterator();
		while (pkgIterator.hasNext()) {
			String pkg_id = pkgIterator.next().getPkg_id();
			PackageVO pkg_temp = pkgRepo.getById(pkg_id);
			pkg_temp.setCurrent_hub("-1");
			pkgRepo.save(pkg_temp);
		}
	}
	//# PUT EXPORT BY PACKAGE
	public void putExport(PackageVO pkg) {
		pkg = pkgRepo.getById(pkg.getPkg_id());
		pkg.setCurrent_hub("-1");
		pkgRepo.save(pkg);
	}
	
	
	//# GET ALL SHIPPERS ARE WORKING IN HUB
	public List<ShipperVO> getShippersInHub(String lghub_id) {
		List<ShipperVO> listShipper = new ArrayList<ShipperVO>();
		List<ShipperVO> shipperInHub = new ArrayList<ShipperVO>();
		
		listShipper = shipperRepo.findAll();
		for(ShipperVO shipper: listShipper) {
			if (shipper.getLghub_id().equals(lghub_id)) {
				shipper.setPasswd("Do you wanna see? Ahihi");
				shipperInHub.add(shipper);
			}
		}
		return shipperInHub;
	}
	
	//# GET ALL PACKAGE IN HUB
	public Object getPackagesInHub(String lghub_id) {
		List<PackageVO> listPkg = new ArrayList<PackageVO>();
		List<PackageVO> listPkgInHub = new ArrayList<PackageVO>();
		
		listPkg = pkgRepo.findAll();
		for(PackageVO pkg: listPkg) {
			if (pkg.getCurrent_hub().equals(lghub_id)) {
				listPkgInHub.add(pkg);
			}
		}
		return listPkgInHub;
	}
	//# GET ROUTE, SHIP ROUTE IN HUB
	public Object getHistoryInHub(String lghub_id, LocalDate date) {
		List<ShipRouteVO> listShipRoute = new ArrayList<ShipRouteVO>();
		List<ShipRouteVO> shipRouteListInDay = new ArrayList<ShipRouteVO>();
		listShipRoute = shiprouteRepo.findAll();
		for (ShipRouteVO s: listShipRoute) {
			if (checkShipRouteHub(lghub_id, s) && s.getDate().compareTo(date) == 0) {
				shipRouteListInDay.add(s);
			}
		}
		List<RouteVO> listRoute = new ArrayList<RouteVO>();
		List<RouteVO> routeListInSpecificDay = new ArrayList<RouteVO>();
		listRoute = routeRepo.findAll();
		for (RouteVO r: listRoute) {
			if (r.getEnd_pos().equals(lghub_id) && r.getEnd_datetime().toLocalDate().compareTo(date) == 0) {
				routeListInSpecificDay.add(r);
			}
			if (r.getStart_pos().equals(lghub_id) && r.getStart_datetime().toLocalDate().compareTo(date) == 0) {
				routeListInSpecificDay.add(r);
			}
		}
		List<Object> resultList = new ArrayList<Object>();
		resultList.add(shipRouteListInDay);
		resultList.add(routeListInSpecificDay);
		return resultList;
	}
	
	//# PREPARE ALL SHIP ROUTE
	@Scheduled(fixedRate = 300000, initialDelay = 2000) // 5 minutes
	public void prepareAllShipRoute () {		
		System.out.println("run ship route");
		List<LogisticHubVO> hub_list = new ArrayList<LogisticHubVO>();
		hub_list = hub_repo.findAll();
		for (LogisticHubVO lghub: hub_list) {
			createShipRouteToday(lghub.getLghub_id());
		}
	}
}
