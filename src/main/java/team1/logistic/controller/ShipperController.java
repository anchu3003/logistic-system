package team1.logistic.controller;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import team1.logistic.entity.PackageVO;
import team1.logistic.entity.ShipRouteVO;
import team1.logistic.service.ShipperService;

/**
 * @author Huan
 *
 */
@CrossOrigin(origins = "http://localhost:3000", allowedHeaders = "*", allowCredentials = "true")
@RestController
@RequestMapping("shipper/")
public class ShipperController {

	@Autowired
	private ShipperService shipperService;

	
	//Test Show all shipper
	@GetMapping("shipper")
	public ResponseEntity<Object> getPackageList(@CookieValue int shipper_id){

	return ResponseEntity.ok(shipperService.getPackageList(shipper_id));
	
	}
	
//	@GetMapping("/package")
//	public ShipRouteVO getPackageListToday(@CookieValue int shipper_id) {
//	
//		return shipperService.getPackageListToday(shipper_id);
//	}

	@GetMapping("/package")
	public Set<PackageVO> getPackageToday(@CookieValue int shipper_id) {
	
		return shipperService.getPackageToday(shipper_id);
	}
	
	
	
	
//	@GetMapping("/history")
//	public Set<PackageVO> getPackageHistory(@CookieValue int shipper_id) {
//	
//		return shipperService.getPackageHistory(shipper_id);
//	}
	
	@GetMapping("/history")
	public Set<ShipRouteVO> getPackageHistory(@CookieValue int shipper_id) {
	
		return shipperService.getPackageHistory(shipper_id);
	}
	
	@PutMapping("/pick.do")
	//@ResponseBody
	public Object getPackageById(@CookieValue String pkg_id, @CookieValue int shipper_id) {
		
		return shipperService.pickPackageById(pkg_id,shipper_id);
	}
	
	@PutMapping("/drop.do")
	//@ResponseBody
	public Object dropPackage(@CookieValue String pkg_id, @CookieValue int shipper_id) {
		
		return shipperService.dropPackageById(pkg_id,shipper_id);
	}
		
}
