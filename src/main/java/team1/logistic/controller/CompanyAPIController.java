package team1.logistic.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import team1.logistic.entity.PackageVO;
import team1.logistic.service.CompanyAPIService;

/**
 * 
 * @author An
 * 
 * */
@CrossOrigin(origins = "http://localhost:3000", allowedHeaders = "*", allowCredentials = "true")
@RestController
@RequestMapping("company_api")
public class CompanyAPIController {
	
	@Autowired
	private CompanyAPIService apiservice;
	
	@RequestMapping(value = "/getTracking", method = RequestMethod.GET)
	@ResponseBody
	public Object getTracking (@RequestBody PackageVO pkg) {
		return apiservice.findTracking(pkg.getPkg_id());
	}
	
	@RequestMapping(value = "/getFee", method = RequestMethod.GET)
	@ResponseBody
	public Object getFee (@RequestBody PackageVO pkg) {
		return apiservice.calculateFee(pkg);
	}
	
	@RequestMapping(value = "/sendPackage", method = RequestMethod.POST)
	@ResponseBody
	public Object sendPackage (@RequestBody PackageVO pkg) {
		return apiservice.savePackage(pkg);
	}
	
}
