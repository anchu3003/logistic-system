package team1.logistic.reponsitory;


import org.springframework.stereotype.Repository;

import team1.logistic.entity.DriverVO;

/**
 * @author The Phap
 */

@Repository
public interface DriverRepositoryCustom  {
	public DriverVO getDriverByStatus(String hub);
}
