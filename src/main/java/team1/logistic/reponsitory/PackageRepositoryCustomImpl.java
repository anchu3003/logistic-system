package team1.logistic.reponsitory;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.springframework.beans.factory.annotation.Autowired;
import team1.logistic.entity.PackageVO;

/**
 * 
 * @author Canh
 * 
 * */

public class PackageRepositoryCustomImpl implements PackageRepositoryCustom{
	
	public PackageRepositoryCustomImpl() {
		super();
	}
	@Autowired(required = true)
	private EntityManager entityManager;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<PackageVO> getPickupPackageList(String lghub_id) {
		String hql = "SELECT pkg FROM PackageVO pkg WHERE pkg.current_hub = :hub AND pkg.current_shipper = :shipper AND pkg.current_driver = :driver AND pkg.sender_address like :lghub_id";
		Query query = entityManager.createQuery(hql);
		query.setParameter("hub", "-1");
		query.setParameter("shipper", -1);
		query.setParameter("driver", -1);
		query.setParameter("lghub_id", "%"+lghub_id+"%"	);
		return  query.getResultList();
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<PackageVO> getDeliveryPackageList(String lghub_id) {
		String hql = "SELECT pkg FROM PackageVO pkg WHERE pkg.current_hub = :lghub_id AND pkg.current_shipper = :shipper AND pkg.receiver_address like :receiver_hub";
		Query query = entityManager.createQuery(hql);
		query.setParameter("lghub_id", lghub_id);
		query.setParameter("receiver_hub", "%"+lghub_id+"%");
		query.setParameter("shipper", -1);
		return query.getResultList();
	}

	
	/**
	 * @author The Phap
	 */
	
	@SuppressWarnings("unchecked")
	@Override
	public List<PackageVO> getListPackageInHub(String lghub_id) {
		String hql ="SELECT pkg FROM PackageVO pkg WHERE pkg.current_hub = :lghub_id";
		Query query = entityManager.createQuery(hql);
		query.setParameter("lghub_id", lghub_id);
		List<PackageVO> list = new ArrayList<PackageVO>();
		list.addAll(query.getResultList());
		return list;
	}

}
