package team1.logistic.reponsitory;

import java.util.List;

import team1.logistic.entity.PackageVO;

/**
 * 
 * @author Canh
 * 
 * */

public interface PackageRepositoryCustom {
	public List<PackageVO> getPickupPackageList(String lghub_id);
	public List<PackageVO> getDeliveryPackageList(String lghub_id);
	public List<PackageVO> getListPackageInHub(String lghub_id);
	
}
