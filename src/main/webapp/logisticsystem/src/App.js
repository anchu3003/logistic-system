import HubHomeComponent from './components/hub/HubHomeComponent'
import HubImportListComponent from './components/hub/HubImportListComponent'
import HubExportListComponent from './components/hub/HubExportListComponent'
import HubPackageInHubComponent from './components/hub/HubPackageInHubComponent'
import HubShipperListComponent from './components/hub/HubShipperListComponent'
import HubHistoryComponent from './components/hub/HubHistoryComponent'
import DividerDriverListComponent from './components/divider/DividerDriverListComponent'
import DividerRoute from './components/divider/DividerRouteComponent'
import DriverHomeComponent from './components/driver/DriverHomeComponent'
import DriverPackageComponent from './components/driver/DriverPackageComponent'
import SignupComponent from './components/account/Sign_upComponent'
import SignOutComponent from './components/webflow/LogoutSucessComponent'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'

function App() {
  return (
      <Router>
          <Switch>
            <Route exact path = "/hub/" component = {HubHomeComponent} ></Route>
            <Route exact path = "/hub/import"  component = {HubImportListComponent} ></Route>
            <Route exact path = "/hub/export"  component = {HubExportListComponent} ></Route>
            <Route exact path = "/hub/package"  component = {HubPackageInHubComponent} ></Route>
            <Route exact path = "/hub/shipper_list"  component = {HubShipperListComponent} ></Route>
            <Route exact path = "/hub/history"  component = {HubHistoryComponent} ></Route>
            <Route exact path ="/divider/route" component ={DividerRoute}></Route>
            <Route exact path ="/divider/driver_list" component ={DividerDriverListComponent}></Route>
            <Route exact path ="/driver/" component ={DriverHomeComponent}></Route>
            <Route exact path = "/driver/container" component = {DriverPackageComponent}></Route>
            <Route exact path = "/account/sign-up" component = {SignupComponent}></Route>
            <Route exact path = "/signout" component = {SignOutComponent}></Route>

          </Switch>
      </Router>
  );
}

export default App;
