/* @author: Canh */


import axios from 'axios';
import HubMenu from '../hub/HubMenuComponent'
import TodayList from '../hub/HubToDayListComponent'
axios.defaults.withCredentials = true;

const HUB_REST_API_URL = 'http://localhost:8080/hub/';

const HubHomeComponent = function(){
        return (    
            <div>
                <HubMenu />
                <TodayList hubview="Total" urlPath={HUB_REST_API_URL} />
            </div>
        )
    }


export default HubHomeComponent