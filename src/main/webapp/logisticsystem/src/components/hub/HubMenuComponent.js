import React from 'react';
import '../hub/css/NavbarCustom.css'

// @author: Canh
 const HubMenu = function() {
    return (
        <div>
            <header className="header" id="header">
            <div className="header_toggle"> <i className='bx bx-menu' id="header-toggle"></i> </div>
            <div className="header_img"> <img src="https://i.imgur.com/hczKIze.jpg" alt="" /> </div>
            </header>
            <div className="l-navbar" id="nav-bar">
                <nav className="nav">
                    <div> <a href="/hub" className="nav_logo"> <i className='bx bx-layer nav_logo-icon'></i> <span className="nav_logo-name">Logistic System</span> </a>
                        <div className="nav_list"> 
                        <a href="/hub/package" className="nav_link"> <i className='bx bx-grid-alt nav_icon'></i> <span className="nav_name">Package</span> </a> 
                        <a href="/hub/import" className="nav_link"> <i className='bx bx-archive-in nav_icon' ></i> <span className="nav_name">Import</span> </a> 
                        <a href="/hub/export" className="nav_link"> <i className='bx bx-archive-out nav_icon' ></i> <span className="nav_name">Export</span> </a> 
                        <a href="/hub/shipper_list" className="nav_link"> <i className='bx bx-user nav_icon'></i> <span className="nav_name">Shippers</span> </a> 
                        <a href="/hub/history" className="nav_link"> <i className='bx bx-folder nav_icon'></i> <span className="nav_name">History</span> </a> 
                        </div>
                    </div> <a href="/hub" className="nav_link"> <i className='bx bx-log-out nav_icon'></i> <span className="nav_name">SignOut</span> </a>
                </nav>
            </div>
        </div>
    );
}

export default HubMenu