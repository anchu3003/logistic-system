import React, { useEffect, useState } from 'react';
import axios from 'axios';

axios.defaults.withCredentials = true;

/* @author: Canh */

const ShipRouteDetail = function(props) {

    return (
        <div className="card bg-light mb-3 shiproute-information" >
            <div className="card-header">
                SHIP ROUTE ID:
                 {
                    (typeof props.shipRouteDetail !== 'undefined' && Object.keys(props.shipRouteDetail).length > 0)
                    ?
                    props.shipRouteDetail.ship_route_id
                    :""
                 }

            </div>
            <div className="card-body">
                <h6 className="card-title">
                {
                    (typeof props.shipRouteDetail !== 'undefined' && Object.keys(props.shipRouteDetail).length > 0) 
                    ?
                    ("Shipper ID: "+ props.shipRouteDetail.shipper_id)
                    :""
                }
                </h6>   
                <p className="card-text">
                    {
                        (typeof props.shipRouteDetail !== 'undefined') 
                        ?
                        (props.shipRouteDetail.packagelist).map( pkg => 
                            <span key={"shpr" + pkg.pkg_id} >{pkg.pkg_id}</span>
                        )
                        :""
                    }

          
                </p>
            </div>
        </div>
    );
};

const ImportShipRouteDetail = function(props) {

    return (

            <div className="overflow-auto">
            <table className="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">SHIP ROUTE ID: 
                                {
                                    (typeof props.shipRouteDetail !== 'undefined' && Object.keys(props.shipRouteDetail).length > 0)
                                    ?
                                    (props.shipRouteDetail.ship_route_id)
                                    :""
                                }
                            </th>
                            <th scope="col">IMPORT ACTION</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            (typeof props.shipRouteDetail !== 'undefined' && Object.keys(props.shipRouteDetail).length > 0) 
                            ?
                            (props.shipRouteDetail.packagelist).map(
                                pkg => 
                                <tr key={"shpr" + pkg.pkg_id}>
                                    <th> Package ID: {pkg.pkg_id}</th>
                                    <td>
                                        <div className="form-check">
                                            <input className="form-check-input" type="checkbox" />
                                        </div>
                                    </td>
                                </tr>
                            )
                            :""
                        }
                    </tbody>
            </table>
            </div>
    );
};

const ExportShipRouteDetail = function(props) {

    return (

        <div className="overflow-auto">
        <table className="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">SHIP ROUTE ID: 
                            {
                                (typeof props.shipRouteDetail !== 'undefined' && Object.keys(props.shipRouteDetail).length > 0)
                                ?
                                (props.shipRouteDetail.ship_route_id)
                                :""
                            }
                        </th>
                        <th scope="col">EXPORT ACTION</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        (typeof props.shipRouteDetail !== 'undefined' && Object.keys(props.shipRouteDetail).length > 0) 
                        ?
                        (props.shipRouteDetail.packagelist).map(
                            pkg => 
                            <tr key={"shpr" + pkg.pkg_id}>
                                <th> Package ID: {pkg.pkg_id}</th>
                                <td>
                                    <div className="form-check">
                                        <input className="form-check-input" type="checkbox" />
                                    </div>
                                </td>
                            </tr>
                        )
                        :""
                    }
                </tbody>
        </table>
        </div>
);
};

const RouteDetail = function(props) {

        return (
            (typeof props.routeDetail !== 'undefined' && Object.keys(props.routeDetail).length > 0)
            ?
            (
                <div className="card bg-light route-information" >
                    
                    
                    <div className="card-header">
                        {(props.routeDetail.start_pos === props.pos) ? "ACTION: EXPORT" :  "ACTION: IMPORT"}
                    </div>
                    <div className="card-body">
                        <h6 className="card-title">ROUTE ID: {props.routeDetail.route_id}</h6>
                        <p className="card-text">
                            <span>Driver: {props.routeDetail.driver_id}</span> <br></br>
                            <span>Start Pos: {props.routeDetail.start_pos}</span> <br></br>
                            <span>End Pos: {props.routeDetail.end_pos}</span> <br></br>
                            <span>Start Time: {props.routeDetail.start_datetime}</span> <br></br>
                            <span>End Time: {props.routeDetail.end_datetime}</span> <br></br>
                        </p>
                    </div>
                    
                </div>
            )
            :""
        );
        
};

const ImportRouteDetail = function(props) {

    return (
        <div className="overflow-auto">
            <table className="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">
                            {(typeof props.routeDetail !== 'undefined' && Object.keys(props.routeDetail).length > 0)
                            ?
                                (props.routeDetail.route_id)
                            :""}
                            </th>
                            <th scope="col">IMPORT ACTION</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            (typeof props.routeDetail.packagelist !== 'undefined' && Object.keys(props.routeDetail).length > 0)
                            ?
                            (props.routeDetail.packagelist).map(
                                pkg => 
                                <tr key={"imr" + pkg.pkg_id}>
                                    <th>Package ID: {pkg.pkg_id}</th>
                                    <td>
                                        <div className="form-check">
                                            <input className="form-check-input" type="checkbox" />
                                        </div>
                                    </td>
                                </tr>
                            )
                            :""
                        }
                    </tbody>
            </table>
        </div>
    );
};

const ExportRouteDetail = function(props) {

    return (
        <div className="overflow-auto">
            <table className="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">
                                {(typeof props.routeDetail !== 'undefined' && Object.keys(props.routeDetail).length > 0)
                                ?
                                    (props.routeDetail.route_id)
                                :""}
                            </th>
                            <th scope="col">EXPORT ACTION</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            (typeof props.routeDetail.packagelist !== 'undefined' && Object.keys(props.routeDetail).length > 0)
                            ?
                            (props.routeDetail.packagelist).map(
                                pkg => 
                                <tr key={"exr"+pkg.pkg_id}>
                                    <th>Package ID: {pkg.pkg_id}</th>
                                    <td>
                                        <div className="form-check">
                                            <input className="form-check-input" type="checkbox" />
                                        </div>
                                    </td>
                                </tr>
                            )
                            :""
                        }
                    </tbody>
            </table>
        </div>
    );
};

const TodayList = function(props) {
    const [allRoute, setAllRoute] = useState([]);
    const [route, setRoute] = useState({});
    const [shipRoute, setShipRoute] = useState({});
    const [schecked, setSChecked] = useState(true);
    const [rchecked, setRChecked] = useState(true);

    useEffect(() => {
        axios.get(props.urlPath).then(response => {
            setAllRoute(response.data);

            if (props.urlPath.includes("import") || props.urlPath.includes("export")) {
                setRoute(response.data[0][0]);
                setShipRoute(response.data[1][0]);
            }
            else {
                (response.data[0][0] != null) ? setRoute(response.data[0][0]) : setRoute(response.data[1][0]);
                setShipRoute(response.data[2][0]);
            }
        });
    },[props.urlPath]);

    return (
        <div className="container">
            {(typeof route !== 'undefined' && Object.keys(route).length > 0)
            ?
                <h1 className="mx-auto text-center">{props.hubview} Route List Today</h1>
            :
                <h1 className="mx-auto text-center">DRIVER REST DAY</h1>
            }

            {(typeof route !== 'undefined' && Object.keys(route).length > 0)
            ?
                <div className="route-list-show row justify-content-around" style={{height: "40vh"}}>
                <div className="route-list col-md-5 mh-100 overflow-auto mb-4">
                    <table className="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col">ROUTE ID</th>
                                <th scope="col">DETAIL</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                allRoute.map(
                                    routes => routes.filter(detail => detail.route_id != null).map(
                                        rdetail => 
                                        <tr key={"route" + rdetail.route_id}>
                                            <th scope="row">{rdetail.route_id}</th>
                                                <td>
                                                    <div className="form-check">
                                                        {
                                                            (rdetail.route_id === route.route_id) 
                                                            ? 
                                                            <input className="form-check-input" type="radio" name="flexRadioDefault" id={"flexRadioDefault" + rdetail.route_id} onClick={() => setRoute(rdetail) } onChange={() => setRChecked(!rchecked)} defaultChecked={rchecked} />
                                                            : 
                                                            <input className="form-check-input" type="radio" name="flexRadioDefault" id={"flexRadioDefault" + rdetail.route_id} onClick={() => {setRoute(rdetail)}}   />
                                                        }
                                                        
                                                    </div>
                                                </td>
                                        </tr>
                                    )
                                )
         
                            }
                        </tbody>
                    </table>
                </div>
                    <div className="route-detail col-md-7">
                        {
                            (props.urlPath.includes("import") || props.urlPath.includes("export"))
                            ?
                            (props.urlPath.includes("import")) ? <ImportRouteDetail routeDetail={route} /> : <ExportRouteDetail routeDetail={route}/>
                            :
                            <RouteDetail routeDetail={route} />
                        }

                    </div>
            </div>
            :""}

            {(typeof shipRoute !== 'undefined' && Object.keys(shipRoute).length > 0)
            ?
            <h1 className="mx-auto text-center">{props.hubview} Ship Route List Today</h1>
            :
            <h1 className="mx-auto text-center">SHIPPER REST DAY</h1>
            }


            {(typeof shipRoute !== 'undefined' && Object.keys(shipRoute).length > 0)
            ?
            <div className="route-list-show row justify-content-around" style={{height: "40vh"}}>
                <div className="route-list col-md-5 mh-100 overflow-auto mb-4">
                    <table className="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col">SHIP ROUTE ID</th>
                                <th scope="col">DETAIL</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                allRoute.map(
                                    routes => routes.filter(detail => detail.ship_route_id != null).map(
                                        detail => 
                                        <tr key={"route" + detail.ship_route_id}>
                                            <th scope="row">{detail.ship_route_id}</th>
                                                <td>
                                                    <div className="form-check">
                                                        {
                                                            (detail.ship_route_id === shipRoute.ship_route_id) 
                                                            ? 
                                                            <input className="form-check-input" type="radio" name="flexRadioDefault2" id={"flexRadioDefault2" + detail.route_id} onClick={() => {setShipRoute(detail)}} defaultChecked={schecked} onChange={() => setSChecked(!schecked)} />
                                                            : 
                                                            <input className="form-check-input" type="radio" name="flexRadioDefault2" id={"flexRadioDefault2" + detail.route_id} onClick={() => {setShipRoute(detail)}}   />
                                                        }
                                                        
                                                    </div>
                                                </td>
                                        </tr>
                                    )
                                )
         
                            }
                        </tbody>
                    </table>
                </div>
                    <div className="route-detail col-md-7 mt-4">
                        {
                            (props.urlPath.includes("import") || props.urlPath.includes("export"))
                            ?
                            (props.urlPath.includes("import")) ? <ImportShipRouteDetail shipRouteDetail={shipRoute} /> : <ExportShipRouteDetail shipRouteDetail={shipRoute}/>
                            :
                            <ShipRouteDetail shipRouteDetail={shipRoute}  />
                        }
                        
                    </div>
            </div>
            :""}
        </div>
    );
};

export default TodayList