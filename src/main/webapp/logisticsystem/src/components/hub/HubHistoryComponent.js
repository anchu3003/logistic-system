import React from 'react';
import axios from 'axios';
import HubMenu from '../hub/HubMenuComponent'
axios.defaults.withCredentials = true
const HUB_REST_API_URL = 'http://localhost:8080/hub/history';
class HubHistoryComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            allroute: []
        }
    }

    componentDidMount() {
        axios.get(HUB_REST_API_URL).then(response => {
                this.setState({allroute: response.data});
            });
    }

    render (){
        return (
            <div>
                <HubMenu />
                <h1>Route List History</h1>
                {
                    this.state.allroute.map(
                        route => route.filter(detail => detail.route_id != null).map(
                            detail => 
                            <p key={detail.route_id}>
                                {detail.route_id}
                                {detail.start_datetime}
                            </p>
                        )
                    )
                }
                <h1>Ship Route List History</h1>
                {
                    this.state.allroute.map(
                        shiproute => shiproute.filter(detail => detail.ship_route_id != null).map(
                            detail =>  
                               <p key={detail.ship_route_id}>
                                 {detail.ship_route_id} 
                                <br></br>
                                {detail.date}
                                </p>    
                        )
                    )
                }
            </div>
        )
    }
}

export default HubHistoryComponent