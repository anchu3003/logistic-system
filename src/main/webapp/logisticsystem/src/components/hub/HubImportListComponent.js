/* @author: Canh */


import React from 'react';
import axios from 'axios';
import HubMenu from '../hub/HubMenuComponent'
import TodayList from '../hub/HubToDayListComponent'
axios.defaults.withCredentials = true
const HUB_REST_API_URL = 'http://localhost:8080/hub/import';
class HubImportListComponent extends React.Component {

    render (){
        return (
            <div>
                <HubMenu />
                <TodayList hubview="Import" urlPath={HUB_REST_API_URL} />
            </div>
        )
    }
}

export default HubImportListComponent