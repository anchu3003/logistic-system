/* @author: Canh */


import React, { useEffect, useState } from 'react';
import axios from 'axios';
import HubMenu from '../hub/HubMenuComponent'
axios.defaults.withCredentials = true
const HUB_REST_API_URL = 'http://localhost:8080/hub/shipper_list';

const ShipperDetail = function(props) {
    return (
        <div className="card bg-light mb-3 shipper-information" >
            <div className="card-header">{props.shipper.name + " (" + props.shipper.staff_id + ")"}</div>
            <div className="card-body">
                <h5 className="card-title">{props.shipper.name}</h5>
                <p className="card-text">
                    <span>Phone Number: {props.shipper.phone_num}</span> <br></br>
                    <span>Role: {props.shipper.role}</span> <br></br>
                    <span>Working Hub: {props.shipper.lghub_id}</span>
                </p>
            </div>
        </div>
    );
};

const HubShipperListComponent = function(props) {
    const [shipperList, setShipperList] = useState([]);
    const [shipper, setShipper] = useState({});
    const [checked, setChecked] = useState(true);
    useEffect(() => {
        axios.get(HUB_REST_API_URL).then(response => {
            setShipperList(response.data);
            setShipper(response.data[0]);
        });
    },[]);

    return (
        <div className="container">
                <HubMenu />
                <h1 className="mx-auto text-center">Shipper List In Hub</h1>
                <div className="shipper-list-show row justify-content-around" style={{height: "60vh"}}>
                <div className="shipper-list col-md-7 mh-100 overflow-auto mb-4">
                    <table className="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col">SHIPPER ID</th>
                                <th scope="col">SHIPPER NAME</th>
                                <th scope="col">DETAIL</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                shipperList.map(
                                    shipperDetail => 
                                    <tr key={"shipper" + shipperDetail.staff_id}>
                                        <th scope="row">{shipperDetail.staff_id}</th>
                                            <td>{shipperDetail.name}</td>
                                            <td>
                                                <div className="form-check">
                                                    {
                                                        (shipperDetail.staff_id === shipper.staff_id) 
                                                        ? 
                                                        <input className="form-check-input" type="radio" name="flexRadioDefault" id={"flexRadioDefault" + shipperDetail.staff_id} onClick={() => {setShipper(shipperDetail)}} defaultChecked={checked} onChange={() => setChecked(!checked)} />
                                                        : 
                                                        <input className="form-check-input" type="radio" name="flexRadioDefault" id={"flexRadioDefault" + shipperDetail.staff_id} onClick={() => {setShipper(shipperDetail)}}   />
                                                    }
                                                    
                                                </div>
                                            </td>
                                    </tr>
                                )
         
                            }
                        </tbody>
                    </table>
                </div>

                <div className="shipper-detail col-md-4">
                    <ShipperDetail shipper={shipper} />
                    </div>
                </div>
            </div>
    );
};

export default HubShipperListComponent