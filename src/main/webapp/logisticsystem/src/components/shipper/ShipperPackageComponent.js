/*

@author: Huan 

*/

import React from "react";
import axios from "axios";
import ShipperMenu from "./ShipperMenuComponent";
import { Route } from "react-router-dom";

axios.defaults.withCredentials = true;

const SHIPPER_REST_API_URL = "http://localhost:8080/shipper/package";
class ShipperPackageComponent extends React.Component {
  constructor(props) {
    super(props);
     this.state = {      
    //shiproute: {},
      packagelist: [],
    };
  }
  componentDidMount() {
    axios.get(SHIPPER_REST_API_URL).then((response) => {
      this.setState({ packagelist: response.data });

      console.log(response.data);
    });
  }
  
 //ship_route.map(route => route.packagelist.map(pkg => pkg.pkg_id))
             // this.state.packagelist.map(
  render() {
    return (
      <div className="container">
      <ShipperMenu />
      <h1 className="mx-auto">Package List To Day</h1>
      <div id="accordion">
          {
              // this.state.shiproute.packagelist.map(
                this.state.packagelist.map(
                  pkg => 
                  <div className="card" key={"sk" + pkg.pkg_id}>
                      <div className="card-header" id={"heading" + pkg.pkg_id}>
                          <h5 className="mb-0">
                              <button className="btn" data-toggle="collapse" data-target={"#" + pkg.pkg_id} aria-expanded="true" aria-controls={pkg.pkg_id}>
                              {pkg.pkg_id}
                              </button>
                          </h5>
                      </div>

                      <div id={pkg.pkg_id} className="collapse" aria-labelledby={"heading" + pkg.pkg_id} data-parent="#accordion">
                          <div className="card-body">
                              <p>Packge ID: {pkg.pkg_id}</p>
                              <p>Created Time: {pkg.created_datetime}</p>
                              <p>Delivery Fee: {pkg.delivery_fee}</p>
                              <p>COD Value: {pkg.cod_value}</p>
                              <p>Receiver Name: {pkg.receiver_name}</p>
                              <p>Receiver Phone Number: {pkg.receiver_phone_num}</p>
                              <p>Receiver Address: {pkg.receiver_address}</p>
                              <p>Sender Name: {pkg.sender_name}</p>
                              <p>Sender Phone Number: {pkg.sender_phone_num}</p>
                          </div>
                      </div>
                  </div>
              )
          }
      </div>
  </div>
    );
  }
}

export default ShipperPackageComponent;
