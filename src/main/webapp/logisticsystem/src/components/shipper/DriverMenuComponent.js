/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';
import '../driver/css/NavbarCustom.css'


 const DriverMenu = function() {
    return (
        <div>
            <header class="header" id="header">
            <div class="header_toggle"> <i class='bx bx-menu' id="header-toggle"></i> </div>
            <div class="header_img"> <img src="https://i.imgur.com/hczKIze.jpg" alt="" /> </div>
            </header>
            <div class="l-navbar" id="nav-bar">
                <nav class="nav">
                    <div> <a href="/driver" class="nav_logo"> <i class='bx bx-layer nav_logo-icon'></i> <span class="nav_logo-name">Logistic System</span> </a>
                        <div class="nav_list"> 
                        <a href="/driver/package" class="nav_link"> <i class='bx bx-grid-alt nav_icon'></i> <span class="nav_name">Package</span> </a> 
                        
                        <a href="/driver/history" class="nav_link"> <i class='bx bx-folder nav_icon'></i> <span class="nav_name">History</span> </a> 
                        </div>
                    </div> <a href="#" class="nav_link"> <i class='bx bx-log-out nav_icon'></i> <span class="nav_name">SignOut</span> </a>
                </nav>
            </div>
        </div>
    );
}

export default DriverMenu