/* @author: Long*/

import React, { useEffect, useState } from "react";
import axios from "axios";
import DriverMenu from "./DriverMenuComponent";
axios.defaults.withCredentials = true;

const DRIVER_REST_API_URL = "http://localhost:8080/driver/container";
const PackageDetail = function (props) {
  return (
    <div className="card bg-light mb-3 package-information">
      <div className="card-header">{props.pkg.pkg_id}</div>
      <div className="card-body">
        <h5 className="card-title">{props.pkg.pkg_id}</h5>
        <p className="card-text">
          <span>Package Id: {props.pkg.pkg_id} </span> <br></br>
          <span>Created Time: {props.pkg.created_datetime} </span> <br></br>
          <span>Size: {props.pkg.size}</span> <br></br>
          <span>Weight: {props.pkg.weight}</span> <br></br>
          <span>Tracking Status: {props.pkg.tracking_status}</span> <br></br>
          <span>Returns: {props.pkg.isreturn}</span> <br></br>
          <span>Delivery Fee: {props.pkg.delivery_fee}</span> <br></br>
          <span>COD Value: {props.pkg.cod_value}</span> <br></br>
          <span>Receiver Name: {props.pkg.receiver_name}</span> <br></br>
          <span>
            Receiver Phone Number: {props.pkg.receiver_phone_num}
          </span>{" "}
          <br></br>
          <span>Receiver Address: {props.pkg.receiver_address}</span> <br></br>
          <span>Sender Name: {props.pkg.sender_name}</span> <br></br>
          <span>Sender Phone Number: {props.pkg.sender_phone_num}</span>{" "}
          <br></br>
          <span>Sender Address: {props.pkg.sender_address}</span> <br></br>
          <span>Current Hub: {props.pkg.current_hub}</span> <br></br>
          <span>Current Shipper: {props.pkg.current_shipper}</span> <br></br>
          <span>Current Driver: {props.pkg.current_driver}</span> <br></br>
          <span>Next Hub: {props.pkg.next_hub}</span> <br></br>
          <span>Pick Time: {props.pkg.pick_time}</span> <br></br>
          <span>Drop Time: {props.pkg.drop_time}</span> <br></br>
        </p>
      </div>
    </div>
  );
};

const DriverRouteInDriverComponent = function (props) {
  const [route, setRoute] = useState({});
  const [packageList, setPackageList] = useState([]);
  const [pkg, setPkg] = useState({});
  const [checked, setChecked] = useState(true);
  useEffect(() => {
    axios.get(DRIVER_REST_API_URL).then((response) => {
        setRoute(response.data);
      //setPackageList(response.data);
      //setPkg(response.data[0]);
    });
  }, []);

  return (
    <div className="container">
      <DriverMenu />
      <h1 className="mx-auto text-center">Package List In Router</h1>
      <div
        className="package-list-show row justify-content-around"
        style={{ height: "60vh" }}
      >
        <div className="package-list col-md-7 mh-100 overflow-auto mb-4">
          <table className="table table-striped table-fixed">
            <thead>
              <tr>
                <th scope="col">ROUTE ID</th>
                <th scope="col">DRIVER ID</th>
                <th scope="col">START POSITION</th>
                <th scope="col">END POSITION</th>
                <th scope="col">START TIME</th>
                <th scope="col">END TIME</th>
                <th scope="col">DETAIL</th>
              </tr>
              <tr>
                <th scope="col">{route.route_id}</th>
                <th scope="col">{route.driver_id}</th>
                <th scope="col">{route.start_pos}</th>
                
                <th scope="col">{route.end_pos}</th>
                <th scope="col">{route.start_datetime}</th>
                <th scope="col">{route.end_datetime}</th>
                <th scope="col">{packageList.pkg_id}</th>
              </tr>
            </thead>
            {/* <tbody>
                            {
                                
                                packageList.map(
                                    pkgDetail => 
                                    <tr key={"pkg" + pkgDetail.pkg_id}>
                                        <th scope="row">{pkgDetail.pkg_id}</th>
                                            <td>{pkgDetail.created_datetime}</td>
                                            <td>{}
                                                <div className="form-check">
                                                    {
                                                        (pkgDetail.pkg_id === pkg.pkg_id) 
                                                        ? 
                                                        <input className="form-check-input" type="radio" name="flexRadioDefault" id={"flexRadioDefault" + pkgDetail.pkg_id} onClick={() => {setPkg(pkgDetail)}} defaultChecked={checked} onChange={() => setChecked(!checked)} />
                                                        : 
                                                        <input className="form-check-input" type="radio" name="flexRadioDefault" id={"flexRadioDefault" + pkgDetail.pkg_id} onClick={() => {setPkg(pkgDetail)}}   />
                                                    }
                                                    
                                                </div>
                                            </td>
                                    </tr>
                                )
         
                            }
                        </tbody> */}
          </table>
        </div>

        {/* <div className="shipper-detail col-md-4">
          <PackageDetail pkg={pkg} />
        </div> */}
      </div>
    </div>
  );
};

export default DriverRouteInDriverComponent;
