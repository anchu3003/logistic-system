import React, { useEffect, useState } from "react";
import axios from "axios";
import DividerMenu from "../divider/DividerMenuComponent";

/*
    @author The Phap 
 */

axios.defaults.withCredentials = true;
const DIVIDER_REST_API_URL = "http://localhost:8080/divider/driver_list";

const DriverDetail = function (props) {
  return (
    <div className="card bg-light mb-3 shadow-lg p-3 mb-5 bg-gray rounded">
      <div className="card-header">
        <h5>
          <b>{props.driver.name + " (" + props.driver.staff_id + ")"}</b>
        </h5>{" "}
      </div>
      <div className="card-body">
        <p className="card-text">
          <span>
            <b>Phone Number: </b> {props.driver.phone_num}
          </span>{" "}
          <br></br>
          <span>
            <b>Current Hub: </b> {props.driver.current_hub}
          </span>{" "}
          <br></br>
          <span>
            <b>Status: </b> {props.driver.status}
          </span>
        </p>
      </div>
    </div>
  );
};
const DividerDriverListComponent = function (props) {
  const [driverList, setDriverList] = useState([]);
  const [driver, setDriver] = useState({});
  const [checked, setChecked] = useState(true);
  useEffect(() => {
    axios.get(DIVIDER_REST_API_URL).then((response) => {
      setDriverList(response.data);
      setDriver(response.data[0]);
    });
  }, []);

  return (
    <div className="container">
      <DividerMenu />
      <h1 className="mx-auto text-center">DRIVER LIST</h1>
      <div
        className="driver-list-show row justify-content-around"
        style={{ height: "60vh" }}
      >
        <div className="driver-list col-md-7 mh-100 overflow-auto shadow-lg p-3 mb-5 bg-white rounded">
          <table className="table table-striped ">
            <thead>
              <tr>
                <th scope="col">DRIVER ID</th>
                <th scope="col">DRIVER NAME</th>
                <th scope="col">DETAIL</th>
              </tr>
            </thead>
            <tbody>
              {driverList.map((driverDetail) => (
                <tr key={"driver" + driverDetail.staff_id}>
                  <th scope="row">{driverDetail.staff_id}</th>
                  <td>{driverDetail.name}</td>
                  <td>
                    <div className="form-check">
                      {driverDetail.staff_id === driver.staff_id ? (
                        <input
                          className="form-check-input"
                          type="radio"
                          name="flexRadioDefault"
                          id={"flexRadioDefault" + driverDetail.staff_id}
                          onClick={() => {
                            setDriver(driverDetail);
                          }}
                          defaultChecked={checked}
                          onChange={() => setChecked(!checked)}
                        />
                      ) : (
                        <input
                          className="form-check-input"
                          type="radio"
                          name="flexRadioDefault"
                          id={"flexRadioDefault" + driverDetail.staff_id}
                          onClick={() => {
                            setDriver(driverDetail);
                          }}
                        />
                      )}
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>

        <div className="shipper-detail col-md-4">
          <DriverDetail driver={driver} />
        </div>
      </div>
    </div>
  );
};

export default DividerDriverListComponent;
