import React, { useEffect, useState } from "react";
import axios from "axios";
import DividerMenu from "./DividerMenuComponent";

/*
    @author The Phap 
 */
axios.defaults.withCredentials = true;
const DIVIDER_REST_API_URL = "http://localhost:8080/divider/route";


const DividerRouteComponent = function (props) {
  const [routeList, setRouteList] = useState([]);
  useEffect(() => {
    axios.get(DIVIDER_REST_API_URL).then((response) => {
      setRouteList(response.data);
    });
  }, []);
  return (
    <div class="container">
      <DividerMenu />
      <h1 class="text-center" style={{ padding: "30px" }}>
        ROUTE LIST
      </h1>
      
      {routeList.map((routeDetail) => (
        <div class="row shadow-lg p-3 mb-5 bg-gray rounded" style={{ border: "1px black solid", marginBottom:"5px", padding:"10px" }}>
          <h4 class="text-center mb-3">Route {routeDetail.start_pos} - {routeDetail.end_pos}</h4>
          <div class=" col-4 ">
            <div className="card bg-light mb-3">
              <div className="card-header"><b>Route ID: {routeDetail.route_id}</b></div>
              <div className="card-body">
                <p className="card-text">
                  <span><b>Star hub: </b> {routeDetail.start_pos}</span> <br></br>
                  <span><b>End hub: </b>{routeDetail.end_pos} </span> <br></br>
                  <span><b>Star Date: </b>{routeDetail.start_datetime} </span>
                </p>
              </div>
            </div>
          </div>
          <div class="col-4" id="accordion">
            <span class="card card-header bg-light"><b>Package List</b></span>
            {routeDetail.packagelist.map((pkg)=>(
            <div class="card">
              <div class="card-header">
                  <button
                    class="btn btn-info"
                    data-toggle="collapse"
                    data-target={"#collapse"+pkg.pkg_id}
                    aria-controls={"collapse"+pkg.pkg_id}
                  >
                   <b>Package ID: {pkg.pkg_id}</b> 
                  </button>
              </div>
              <div
                id={"collapse"+pkg.pkg_id}
                class="collapse"
                style={{width:"100%"}}>
                <div className="card-body">
                  <span><b>Created date: </b> {pkg.created_datetime}</span> <br></br>
                  <span><b>Current hub: </b>{pkg.current_hub} </span> <br></br>
                  <span><b>Next hub: </b>{pkg.next_hub} </span> <br></br>
                  <span><b>From </b>{pkg.sender_address} </span> <br></br>
                  <span><b>To </b>{pkg.receiver_address} </span> <br></br>
                </div>
              </div>
            </div>
            ))}
          </div>
          <div class="col-4">
            <div className="card bg-light mb-3">
              <div className="card-header">
                <form>
                  <span><b>Driver ID:   </b></span>
                  <span>{routeDetail.driver_id}</span>
                </form>
              </div>
              <div className="card-body d-flex flex-row-reverse">
              <button type="button" class="btn btn-info" style={{width:"30%", margin:"5px"}}>Submit</button>
              <button type="button" class="btn btn-info" style={{width:"30%", margin:"5px"}}>Edit</button>
              </div>
            </div>
          </div>
        </div>
      ))}
    </div>
  );
};


export default DividerRouteComponent;
